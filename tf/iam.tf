data "aws_iam_policy" "AmazonSESFullAccess" {
  arn = "arn:aws:iam::aws:policy/AmazonSESFullAccess"
}

resource "aws_iam_role" "lambda_ses_forwarder" {
  name               = "lambda-ses-forwarder-${var.domain}"
  assume_role_policy = file("templates/iam-lambda-trust.json")

  inline_policy {
    name   = "ses-forwarder"
    policy = templatefile("templates/iam-ses-forwarder-policy.json", { bucket = "${var.domain}-email" })
  }
}

resource "aws_iam_role_policy_attachment" "lambda_ses_forwarder_ses_full_access" {
  role       = aws_iam_role.lambda_ses_forwarder.name
  policy_arn = data.aws_iam_policy.AmazonSESFullAccess.arn
}

resource "aws_iam_role" "lambda_at_edge" {
  name               = "${var.domain}-lambda-edge"
  assume_role_policy = file("templates/iam-lambda-edge-trust.json")

  inline_policy {
    name   = "${var.domain}-logs"
    policy = file("templates/iam-logs-policy.json")
  }
}
