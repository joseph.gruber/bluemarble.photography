---
title: Hello Spring!
seo_title: Hello Spring! April Photo of the Month
description: Well hello spring! The crack of the bat. Birds singing in the morning. Cherry blossoms blooming. And a free April photo of the month download for you!
date: 2017-04-01T11:59:00Z
featured_image:
  filename: JosephGruber_20150412_064849.jpg
  caption: Sunshine reflects off of the water in the Tidal Basin with cherry blossoms seen in the foreground
gallery_name: hello-spring
allow_download: watermark
---

Well, hello spring! The crack of the bat. Birds singing in the morning. Cherry blossoms blooming. And a free April photo of the month download for you! Use as your new desktop background or smartphone wallpaper. Share on social media or just print it out and hang at your desk.

Click here to download. Additional resolutions available by clicking the image below.
