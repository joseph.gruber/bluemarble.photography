---
title: Winter in DC
seo_title: Bomb Cyclone? Nor' Easter? Winter Storm Grayson?
description: Whether you call it bombogenesis, bomb cyclone, or nor'easter - one thing is for sure winter in DC is cold! Cold enough for the DC memorials and monuments to share the stage with some pretty solid ice.
date: 2018-01-07T01:26:00Z
featured_image:
  filename: JosephGruber_20180106_142037.jpg
  caption: The sun shines overhead on a ice covered, frozen Potomac River
gallery_name: winter-in-dc
---

Whatever you call it – [bombogenesis](https://www.washingtonpost.com/news/capital-weather-gang/wp/2018/01/03/no-need-to-duck-and-cover-this-is-the-bomb-cyclone-explained/){:target="\_blank" rel="noopener"}, nor’easter, or winter storm Grayson – one thing is for sure. It’s cold! While we get our fair share of winter storms from [time](https://flic.kr/p/DuKn7p){:target="\_blank" rel="noopener"} to [time](https://flic.kr/p/DuKn7p){:target="\_blank" rel="noopener"}, winter in DC isn’t quite known for the inches of snow left behind. More often than not it’s the deep freezes we find ourselves in. And these past two weeks of below freezing temperatures throughout the area sure has left the city entirely frozen!

Two to three days of below freezing temperatures are enough to start leaving the Lincoln Memorial Reflecting Pool, Potomac River, and Capitol Reflecting Pool with a layer of ice. Some people are usually crazy enough to try to walk on them despite the many [safety](https://flic.kr/p/DpTt5b){:target="\_blank" rel="noopener"} warnings. But after this extended period of sub-freezing temperatures, one thing is for sure, the smaller pools of water are indeed frozen. Maybe not the Potomac River quite yet, but the Lincoln Memorial Reflecting Pool is a solid block of ice from top to bottom.

And of course, that makes for some pretty amazing photos of the sights around [DC](https://bluemarble.photography/washington-dc/). From the Lincoln Memorial to the Washington Monument and the Pentagon, winter in DC is all about the ice this year. At least so far!
