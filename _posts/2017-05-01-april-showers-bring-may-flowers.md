---
title: April Showers Bring May Flowers
description: April showers bring May flowers. And also a new wallpaper download for you! Enjoy this image of wildflowers in bloom along the California coastline.
date: 2017-05-01T06:30:00Z
featured_image:
  filename: JosephGruber_20150425_112734.jpg
  caption: Dozens of orange and yellow tulips in bloom
gallery_name: april-showers-bring-may-flowers
allow_download: watermark
---

Happy May! As the saying goes, April showers bring May flowers. Plus it also brings you a new photo of wildflowers in bloom!

There wasn't much in the way of rain this April, but the [flowers](https://bluemarble.photography/stormy-dc-sunset/#gallery/371ecc1e9ed5f3716997f831008f5cbf/573/cart) are certainly in bloom. And forecasters are calling for May to potentially be the first [wetter-than-average](https://www.washingtonpost.com/news/capital-weather-gang/wp/2017/04/28/may-could-be-the-first-wetter-than-average-month-in-d-c-since-june/){:target="\_blank" rel="noopener"} month in Washington, D.C. since May of 2016. Maybe we should just change the phrase to May showers and May flowers?

So enjoy this [free download](https://bluemarble.photography/digital-downloads/?order=4bec2c76b6169fcb6d4e0595022a326d) for the month. Use it as your new smartphone background or your desktop wallpaper. Additional resolutions are available by clicking the image below.
