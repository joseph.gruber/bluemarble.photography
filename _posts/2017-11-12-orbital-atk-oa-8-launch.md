---
title: Orbital ATK OA-8 Launch
date: 2017-11-12T19:39:00Z
featured_image:
  filename: JosephGruber_20171112_062311.jpg
  caption: Antares rocket standing on the launch pad, ready for liftoff, as sun breaks on the horizon
gallery_name: orbital-atk-oa8
---

A successful launch, after scrubbing on the first attempt, of the [Orbital ATK OA-8](https://www.orbitalatk.com/news-room/feature-stories/OA8-Mission-Page/default.aspx){:target="\_blank" rel="noopener"} commercial resupply services mission to the International Space Station (ISS)!
