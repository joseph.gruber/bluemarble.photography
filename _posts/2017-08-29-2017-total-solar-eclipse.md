---
title: 2017 Total Solar Eclipse
description: For over a year, I had planned for the 2017 total solar eclipse. However, at the moment of totality, I realized I hadn't prepared at all. Not for this.
date: 2017-08-29T01:08:00Z
featured_image:
  filename: JosephGruber_20170821_143451.jpg
  caption: Full totality of the sun as the moons passes in front during a total solar eclipse
gallery_name: 2017-total-solar-eclipse
---

The final countdown started. The sky grew darker in every direction. 10, 9, 8... Look, there's Venus! 7, 6, 5... Glasses off, glasses off! 4, 3, 2... Woah, look at that diamond ring! 1. For over a year, I had planned for the 2017 total solar eclipse. However, at the moment of totality, I realized I hadn't prepared at all. Not for this.

They say that seeing a total solar eclipse is an [emotional experience](https://xkcd.com/1880/){:target="\_blank" rel="noopener"}. Who am I to disagree? I stood there, eyes to our closest star which was seemingly no longer there, among friends and dozens of strangers, awestruck and without words. Still, now, it's hard to explain to someone who has not seen totality what an amazing and inspiring sight it is. Having seen several solar and [lunar eclipses](https://www.flickr.com/photos/josephgruber/15476883622/){:target="\_blank" rel="noopener"} previously, I was one of those people before 2:33 PM on August 21, 2017. And [partial solar eclipses](https://www.flickr.com/photos/josephgruber/15425420819){:target="\_blank" rel="noopener"} are already a mesmerizing event to see with your own eyes - provided they are protected by solar glasses that is!

Throngs of eclipse watchers had already started arriving hours before. As one of only two national parks across the United States within the path of totality, the [Great Smoky Mountains National Park](https://www.nps.gov/grsm/index.htm){:target="\_blank" rel="noopener"} was clearly the place to be to experience the 2017 total solar eclipse in all of nature's wonder. With vast mountainous vistas in the distance and valleys of forest below, the great outdoors was the perfect complement to this rare celestial event.

It was blisteringly hot. The sky was for the most part clear. And the sun, as if on center stage, shined strong over those ready to watch the slow dance between the moon and sun.

I setup my photo gear and spent several minutes lining up and focusing my camera with the soon to be eclipsed sun. The list of shots I had planned for several months was in my head. On my iPhone was a cheat sheet of exposure levels for the various eclipse phases.

At precisely 1:05 PM, I look up through my filtered binoculars and see, well, the same ol' sun. But a few seconds later, ever so slowly, the moon began to take a small bite out of the sun. The 2017 total solar eclipse had started! I shared my binoculars with friends and strangers so they could experience their first view of the moon blocking the sun. Pointing out the sunspots in the center of the sun, they slowly disappeared over the next ninety minutes.

Beyond photographing the 2017 total solar eclipse, I wanted to experience the various phenomena. I wanted to see the [shadow bands](https://eclipse2017.nasa.gov/exploring-shadow-bands){:target="\_blank" rel="noopener"}. And the crescent sun on the ground filtered through the leaves of the trees. I wanted to hear the crickets and birds as day turned into night. I wanted to see the planets - Mars, Venus, Mercury, and Jupiter. Moreso than the days leading up, I now really wanted it to [drop a few degrees](https://eclipse2017.nasa.gov/temperature-change-during-totality){:target="\_blank" rel="noopener"}!

Before the eclipse, I had watched some videos of eclipse watchers shouting out in sudden joy when they saw the various features including the sight of [Bailey's beads](https://eclipse2017.nasa.gov/bailys-beads-effect){:target="\_blank" rel="noopener"} and the diamond ring. Before I experienced my first total solar eclipse, it seemed as if these individuals were just overly excited eclipse chasers. But there I stood, as totality began to sweep over us and the sun's corona shone bright in the sky where the sun was just moments before, doing the same exact thing as the joy of totality completely overtook me.

Blinded not by the brightness of the sun but the magnitude of the experience, I had forgotten every single thing I wanted to do as a photographer. At some point, I had somehow even managed to reduce the focal length of my camera from 840mm to 260mm! For a large telephoto lens, this is something that would be quite noticeable, but all thought had left my brain at this point. I saw Venus; I heard the cricket's chirp. About halfway through totality, the deep orange of sunset low on the horizon caught my eye. I spun around to take in a full 360-degree sunrise/sunset. I gazed up to where the sun once was, mouth agape, staring at the brilliance of the corona.

Two minutes and twenty seconds might seem like a reasonably long period of time. Diamond ring! Diamond ring! And then, as suddenly as it began, the sky was brightening, and it was over.

Despite all of my preparation, nothing could have prepared me for my first total solar eclipse. Now, I try to explain what totality is like to those who didn't have the opportunity to experience it themselves. But I don't do it justice. No words will. No photos will. You have to spend a relatively brief moment being just a frustratingly small part of an incredibly large universe. Now, [when is the next one](https://en.wikipedia.org/wiki/List_of_solar_eclipses_in_the_21st_century){:target="\_blank" rel="noopener"}?
