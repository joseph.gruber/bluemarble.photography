---
title: DC March for Science
seo_title: DC March for Science Rally and Protest
description: Protesters gathered and marched in the United States Capitol for the DC March for Science to show support for science funding and communication
date: 2017-04-25T22:56:00Z
featured_image:
  filename: JosephGruber_20170422_150205.jpg
  caption: Hundreds of demonstrators march in front of the United States Capitol with signs in support of science held high
gallery_name: dc-march-for-science
---

Not long after President Trump took office, references to climate change seemed to disappear from the White House website. An off-the-cuff comment quickly appeared on Reddit, "[There needs to be a Scientists' March on Washington](https://www.reddit.com/r/politics/comments/5p5civ/all_references_to_climate_change_have_been/dcojgl0/?st=j1y3b5j8&sh=278753a2){:target="\_blank" rel="noopener"}." A Facebook event was soon created and three months later an idea grew into a global rally for science. On Earth Day, science supporters converged for the DC March for Science along with over 600 satellite marches.

Despite the rain, thousands of people gathered to take part in a rally and teach-in at the Washington Monument. Bill Nye andvother celebrities joined supporters in DC to rally and champion for "[robustly funded and publicly communicated science](https://www.marchforscience.com/mission/){:target="\_blank" rel="noopener"}". Politicians, including Representative Don Beyer (D-Va), also spoke to constituents to kickoff the March for Science. Rep. Beyer spoke to protecting science from partisan politics and noted, "Science makes us free. Science makes us powerful."

Later, marching down Pennsylvania Avenue to the United States Capitol, supporters with signs held high were heard chanting, "We're nerds. We're wet. And we're very, very upset."
