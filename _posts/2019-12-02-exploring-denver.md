---
title: Exploring Denver
description: From brilliant, snow filled sunrises at Red Rocks Park to majestic sunsets over the Front Range, the views never stop when exploring Denver in early winter
date: 2019-12-02T21:30:00Z
featured_image:
  filename: JosephGruber_20191110_130735.jpg
  caption: Bright orange, purple and blue colors fill the sky from the horizon to the heavens at sunrise in Denver
gallery_name: 2019-exploring-denver
---
Hands down, Red Rocks Park and Ampitheatre is one of the coolest places to checkout when in Denver. From brilliant, snow-dusted sunrises to endless horizons to the east over the plains of Denver and Colorado, the views never stop when exploring Denver in early winter. And later in the evening, don't miss the majestic sunsets over the Front Range to the west!
