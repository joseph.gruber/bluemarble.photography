---
title: Assateague Island Adventures
description: Wild horses, dark night skies, white sand beaches, beautiful sunrises and amazing sunsets. All found at Assateague Island National Seashore!
date: 2017-04-20T00:32:00Z
featured_image:
  filename: JosephGruber_20170416_064710.jpg
  caption: Sunset over the sandy beaches at Assateague Island National Seashore
gallery_name: assateague-island-adventures
---

It's spring break weekend! So, what else would a DC-area photographer do except camp at a national seashore with photographers from [DC Focused](http://www.dcfocused.com/){:target="\_blank" rel="noopener"}? Roughly 37 miles long, Assateague Island runs along the eastern shore of the Delmarva Peninsula and consists of two sections. Assateague Island National Seashore, run by the [National Park Service](https://www.nps.gov/asis/index.htm){:target="\_blank" rel="noopener"}, in Maryland and Chincoteague National Wildlife Refuge in Virginia. While many were spending their Easter weekend in Ocean City just five miles to the north, the beaches at Assateague were practically desolate.

Friday evening started off with a magnificent sunset that even included a [sun halo](http://earthsky.org/space/what-makes-a-halo-around-the-moon){:target="\_blank" rel="noopener"}! Of course, I couldn't pass up an opportunity to capture the stars in the night sky. Since the moon was just past full, the Milky Way wasn't visible but the moon rise left me speechless. And what else do photographers do when they can't shoot the night sky? Break out the steel wool and do some light painting on the beach of course!

Oh, and if Assateague Island is ringing a bell it's probably because of the historic ponies. Not the same as the Chincoteague ponies driven annually across Assateague channel, the [Assateague horses](https://www.nps.gov/asis/learn/nature/horses.htm){:target="\_blank" rel="noopener"} are wild and are specific to the Maryland side of the island. And while wild, if you leave your car door open they may still come up and investigate!

While a busy park in the summer with its beaches and short distance from DC, if able during shoulder season, take a trip to visit this stunning national seashore.
