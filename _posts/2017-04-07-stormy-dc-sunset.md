---
title: Stormy DC Sunset
seo_title: Stormy DC Sunset on the National Mall
description: Passing storms light up the sky orange as a stormy DC sunset provided for a colorful and changing sky over the National Mall.
date: 2017-04-07T12:17:00Z
featured_image:
  filename: JosephGruber_20170406_193340.jpg
  caption: An orange sky appears, with the Washington Monument off in the distance, after rain storms pass through the DC area
gallery_name: stormy-dc-sunset
---

Yesterday evening, storms continued to move through the D.C. area putting on a beautiful show just before sunset. It was amazing how much the sky and clouds changed over the hour just before the sun went down. The [United States Capitol Building](https://www.aoc.gov/us-capitol-building){:target="\_blank" rel="noopener"} always make for a great photography outing with so many different views around the Capitol grounds. A deluge of rain was just the cherry on top for this stormy DC sunset!
