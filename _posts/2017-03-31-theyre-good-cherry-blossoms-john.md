---
title: They're Good Cherry Blossoms, John
description: From an abnormally warm winter to a late snowstorm, the cherry blossoms in DC kept us on the edge of our seats. But peak bloom sure didn't disappoint!
date: 2017-03-31T23:58:00Z
featured_image:
  filename: JosephGruber_20170225_083006.jpg
  caption: A fully bloomed cherry blossom tree leads the eye to the blue sky above
gallery_name: 2017-cherry-blossoms
---

Not everyone appreciates the cherry blossom season here in Washington, D.C. ([I'm looking at you Josh](https://www.washingtonpost.com/news/capital-weather-gang/wp/2017/03/30/i-hate-the-cherry-blossoms-there-i-said-it/){:target="\_blank" rel="noopener"}). But it's undoubtedly one of the prettiest times of the year in this area bested perhaps only by our fall foliage. Sure, peak bloom brings tourists - lots of them - as well as locals who usually don't venture out of the suburbs. But the beauty of the cherry blossoms sure makes up for those minor inconveniences.

Due to the abnormally warm winter the D.C. area had, the cherry trees began blooming unusually early. By mid-February the buds and florets were visible. And by the end of February, some of the [varieties](https://www.nps.gov/subjects/cherryblossom/types-of-trees.htm){:target="\_blank" rel="noopener"} of cherry trees had reached peak bloom! By early March the National Park Service had even [announced](http://www.nationalcherryblossomfestival.org/visitor-information/bloom-watch/){:target="\_blank" rel="noopener"} that peak bloom would be one of the earliest ever.

But after our abnormally warm winter, a sustained period of cold weather settled in during early March. A late winter snowstorm even passed through covering the blossoms in ice and snow. Forecasters [expected](https://www.washingtonpost.com/news/capital-weather-gang/wp/2017/03/09/cherry-blossoms-could-be-seriously-damaged-by-upcoming-cold-snap/){:target="\_blank" rel="noopener"} that even a 30-minute drop below 28°F would damage over 10% of the cherry blossoms. Ultimately, the temperature dropped below 25°F over the course of the week. Many expected greater than 90% damage to the blossoms.

Thankfully though we made it through our cold snap and spring finally arrived. While several trees did end up losing their blossoms, the over 3,700 cherry trees lined around the Tidal Basin, and Hains Point allowed for another magnificent peak bloom! From the vivid pink flowers of the Okame Cherry trees to the abundance of Yoshino Cherry trees, you can't miss them.

Definitely add coming to Washington, D.C. in the spring to see the cherry blossoms to your list. Come run a 10k or enjoy the parade or just take a stroll around the Tidal Basin. No matter what, I'll guarantee you won't be disappointed. 12/10 would see again!
